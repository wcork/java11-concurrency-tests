# Some Java 11 Concurrency tests

Within src/Main.java you will see some tests of Java 11 parallel streams.

More tinkering and examples will be made for reference later.

Run this test with `gradlew run`