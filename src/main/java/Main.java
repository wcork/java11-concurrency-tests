
import java.io.IOException;
import java.util.List;
import java.util.OptionalLong;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("Parallel Stream Test");

        // boxed() converts IntStream to Stream<Integer>. This is needed to collect into List.
        // This is for demo purposes. In a real scenario, we can just operate on the stream.
        List<Integer> testList = IntStream.rangeClosed(1,10).boxed().collect(Collectors.toList());

        System.out.println("In order for loop:");
        for (int i = 0; i < testList.size(); i++) {
            System.out.println("" + i + ": " + testList.get(i));
        }

        System.out.println("In order list forEach:");
        testList.forEach(e -> System.out.println("" + testList.indexOf(e) + ": " + e));

        System.out.println("Parallel list forEach:");
        testList.parallelStream().forEach(e -> System.out.println("" + testList.indexOf(e) + ": " + e));

        // Notice if using a ForkJoinPool, that even if the parallelism is 1, the order of operations
        //  will not result in the same order as the list due to the parallel stream.
        // This also removes the parallel operation from the common fork-join thread pool (useful for async).
        ForkJoinPool myPool = new ForkJoinPool(12);
        System.out.println(String.format("Parallel forEach using specified ForkJoinPool (size %d):",
                myPool.getParallelism()));
        myPool.submit(() ->
                testList.parallelStream().forEach(e ->
                    System.out.println("" + testList.indexOf(e) + ": " + e)
        )).get();

        System.out.println("Large Async Parallel Tasks");

        // Let's do this asynchronously with 2 data sets using CompletableFuture.
        // We will actually do some computations in this one.
        IntStream large_is1 = IntStream.rangeClosed(1,10000);
        IntStream large_is2 = IntStream.rangeClosed(10001,20000);

        // This future uses the common fork-join pool
        CompletableFuture<Long> result1 = CompletableFuture.supplyAsync(() -> {
            // Reduce uses OptionalInt. Here is one way of handling that by defaulting to zero.
            return large_is1.parallel().mapToLong(x->x).reduce(0, Long::sum);
        });

        // This future uses the previously created fork-join pool.
        CompletableFuture<Long> result2 = CompletableFuture.supplyAsync(() -> {
            // Reduce uses OptionalInt. This way, we handle it via exceptionally clause.
            OptionalLong opt_sum = large_is2.parallel().mapToLong(x->x).reduce(Long::sum);
            if (opt_sum.isPresent()) {
                return opt_sum.getAsLong();
            } else {
                // Throw a CompletionException. It requires wrapping around another.
                throw new CompletionException(new IOException());
            }
        }, myPool).exceptionally(e -> {
            System.out.println("There was an error with Stream2");
            return 0L;
        });
        System.out.println("Stream1 sum: " + result1.get());
        System.out.println("Stream2 sum: " + result2.get());
    }
}
